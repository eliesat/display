#!/bin/sh

# Configuration
#########################################
plugin="bg"
version="green-art"
targz_file="$plugin-$version.tar.gz"
git_url="https://gitlab.com/eliesat/display/-/raw/main/bg"
url="$git_url/$targz_file"
temp_dir="/tmp"

#check and_remove package old version
#########################################
check_and_remove_package() {
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/AJPan/eliesat-panel/icons-default/bg* > /dev/null 2>&1
}
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
}
cleanup
    