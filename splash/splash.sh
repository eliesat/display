#!/bin/bash

device=$(head -n 1 /etc/hostname)
image='splash'
version='vu+'

#determine image url
case $device in
vuzero4k)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vuzero4k-splash_usb.zip';;

vuuno4kse)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vuuno4kse-splash_usb.zip';;

vuuno4k)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vuuno4k-splash_usb.zip';;

vusolo4k)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vusolo4k-splash_usb.zip';;

vuduo4kse)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vuduo4kse-splash_usb.zip';;

vuduo4k)
url='https://gitlab.com/eliesat/display/-/raw/main/splash/vuduo4k-splash_usb.zip';;

*) echo "> your device is not supported "
exit 1 ;;
esac

#check mounted storage
msp=("/media/hdd" "/media/usb" "/media/usb")

for ms in "${msp[@]}"; do
if [ -d "$ms" ]; then
echo ""
break
fi
done

if [ -z "$ms" ]; then
echo "> Mount your external memory and try again"
exit 1
else

#check images path
if [ -d $ms/images ]; then
echo ""
else
echo ""
mkdir $ms/images
fi


echo '> Downloading '$image'-'$version' image to '$ms'/images please wait...'
sleep 3s

wget -O $ms/images/$device-splash_usb.zip $url

echo "> Download $image-$version image to '$ms'/images finished..."
sleep 3
echo "> uploaded by eliesat"
sleep 1

fi
