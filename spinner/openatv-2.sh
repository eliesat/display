#!/bin/sh

if grep -qs -i "openpli" /etc/issue; then
dir1=/usr/share/enigma2/skin_default/spinner/original
dir2=/usr/share/enigma2/skin_default/spinner
else
dir1=/usr/share/enigma2/spinner/original
dir2=/usr/share/enigma2/spinner
fi

if [ -d $dir1 ]; then
echo "> removing spinner please wait..."
sleep 3s 
rm -rf $dir2/*.png > /dev/null 2>&1
cp $dir1/*.png $dir2/
rm -rf $dir1 > /dev/null 2>&1
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3

else

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

#config
plugin=openatv
version=2
url=https://gitlab.com/eliesat/display/-/raw/main/spinner/$plugin-$version.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz


if [ ! -d $dir1 ]; then
mkdir $dir1
fi
cp $dir2/*.png $dir1/
rm -rf $dir2/*.png > /dev/null 2>&1

#download & install
echo "> Downloading $plugin-$version spinner please wait ..."
sleep 3

wget --show-progress -qO $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

if grep -qs -i "openpli" /etc/issue; then
mv /usr/share/enigma2/spinner/*  /usr/share/enigma2/skin_default/spinner/
rm -rf /usr/share/enigma2/spinner
fi

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version spinner installed successfully"
echo "> Uploaded By ElieSat"
sleep 3

else

echo "> $plugin-$version spinner installation failed"
sleep 3
fi

fi