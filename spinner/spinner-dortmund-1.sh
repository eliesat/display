#!/bin/sh

if grep -qs -i "openpli" /etc/issue; then
dir1=/usr/share/enigma2/skin_default/spinner/original
dir2=/usr/share/enigma2/skin_default/spinner
else
dir1=/usr/share/enigma2/spinner/original
dir2=/usr/share/enigma2/spinner
fi

if [ -d $dir1 ]; then
echo "> removing spinner please wait..."
sleep 3s 
rm -rf $dir2/*.png > /dev/null 2>&1
cp $dir1/*.png $dir2/
rm -rf $dir1 > /dev/null 2>&1
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

#config
plugin=spinner-dortmund
version=1
url=https://gitlab.com/eliesat/display/-/raw/main/spinner/$plugin-$version.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz


if [ ! -d $dir1 ]; then
mkdir $dir1
fi
cp $dir2/*.png $dir1/
rm -rf $dir2/*.png > /dev/null 2>&1

#download & install
echo "> Downloading $plugin-$version  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

if grep -qs -i "openpli" /etc/issue; then
mv /usr/share/enigma2/spinner/*  /usr/share/enigma2/skin_default/spinner/
rm -rf /usr/share/enigma2/spinner
fi

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s

else

echo "> $plugin-$version installation failed"
sleep 3s
fi

fi
exit 0
