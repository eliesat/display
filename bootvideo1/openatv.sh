#!/bin/sh

# Configuration
#########################################
plugin="openatv"
git_url="https://gitlab.com/eliesat/display/-/raw/main/bootvideo1"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/share/oatv-bootlogo"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

#check arch armv7l aarch64 mips 7401c0 sh4
#########################################
arch=$(uname -m)
sleep 1
if [ "$arch" == "armv7l" ]; then
device=supported
sleep 3
else
echo "> your device is not supported"
sleep 3
exit 1
fi

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d /usr/share/oatv-bootlogo ]; then
if [ -z "$D" -a -f "/etc/init.d/oatv-bootlogo" ]; then
 chmod 755 /etc/init.d/oatv-bootlogo > /dev/null 2>&1
	/etc/init.d/oatv-bootlogo stop
fi

if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-f -r $D"
	else
		OPT="-f"
	fi
	update-rc.d $OPT oatv-bootlogo remove
 rm -rf /etc/init.d/oatv-bootlogo > /dev/null 2>&1
 rm -rf /usr/share/oatv-bootlogo > /dev/null 2>&1
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3
exit 1
fi
}
check_and_remove_package

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
chmod 755 /etc/init.d/oatv-bootlogo > /dev/null 2>&1
chmod 755 /usr/share/oatv-bootlogo/oatv-bootlogo > /dev/null 2>&1
if type update-rc.d >/dev/null 2>/dev/null; then
	if [ -n "$D" ]; then
		OPT="-r $D"
	else
		OPT="-s"
	fi
	update-rc.d $OPT oatv-bootlogo start 70 S . stop 89 0 .
fi
  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By ElieSat"
print_message "> Ur device may reboot now please wait ..."
sleep 1
reboot
}
cleanup
    