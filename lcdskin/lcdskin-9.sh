#!/bin/sh

if [ -d /usr/share/enigma2/display/OE-A_LCDSkin_9 ]; then
echo "> removing lcdskin please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/display/OE-A_LCDSkin_9 > /dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

#config
plugin=lcdskin
version=9
url=https://gitlab.com/eliesat/display/-/raw/main/lcdskin/$plugin-$version.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
echo "> $plugin-$version installed successfully"
echo "> Uploaded By ElieSat"
sleep 3s

else

echo "> $plugin-$version installation failed"
sleep 3s
fi

fi
exit 0
